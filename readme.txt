Minimale Ausgaben auf Konsole:

java -jar sftk.jar test -e env-rdev.json -s fsfa-tests.json -x junit-testresults.xml
java -jar sftk.jar test -e env-rdev.json -s rdev_all_tests.json -v -x test-results.xml -c coverage.xml

Verbose Ausgaben auf Konsole:

-v hinzufügen

Coverage Informationen per HTML:

-c coverageReport.html

Coverage Information per Clover XML:

-c coverageReport.xml

