SFTK Version 1.4
=================

## New Features:

- `--soql`: instead of using `-s|--scheme` you can define a SOQL Query to fetch ApexTestClass names. e.g.: 

		java -jar sftk.jar test -e env-file.json --soql "SELECT Name FROM ApexClass WHERE Name like 'YSL_%Test'"

- `--sosl`: like `--soql` but uses salesforce SOSL Queries. Pass empty string to use default query: `FIND {@IsTest} IN ALL FIELDS RETURNING ApexClass (Name)` e.g.

		java -jar sftk.jar test -e env-file.json --sosl ""

- added some more command line parameter help details
- added check in salesforce runTest response for required field `totalTime` and added more error details if missing