#!/bin/bash
removeInvalidFolderAndFiles () 
{
	isValid=invalid
	for n in "${deploymentFolders[@]}"
	do
		if [ "$d" == "$n" ]
		then
				isValid=valid
				removeFiles $d
		fi
		if [ $isValid == invalid ] && [ $n == ${deploymentFolders[${#deploymentFolders[@]}-1]} ]
		then
				rm -rf "$d"
		fi
	done
	
}

removeFiles () 
{	
	cd $d
	
	for n in "${notDeployFiles[@]}"
	do
		rm -f "$n"
	done
	
	if [ "$d" == "layouts" ]
	then
		for f in *.layout 
		do
			grep -v -i "VERSION.TITLE VERSION.CREATED_DATE VERSION.LAST_UPDATE" "${PWD}/$f" > "${PWD}/$f"_
			rm -f "${PWD}/$f%"
			mv "${PWD}/$f"_ "${PWD}/$f"
		done	
	fi
	cd ..

}

createPackageXML () {
	echo create package xml...
	writePackageHeader
	writePackageTypes
	writePackageFooter
}

writePackageHeader () {
	(
		echo '<?xml version="1.0" encoding="UTF-8"?>'
		echo '<Package xmlns="http://soap.sforce.com/2006/04/metadata">'
	)>package.xml
}

writePackageTypes () {
	for n in "${packageTypes[@]}" 
	do 
	(
		echo "${tab}<types>"
		echo "${tab}${tab}<members>*</members>"
		echo "${tab}${tab}<name>$n</name>"
		echo "${tab}</types>"
	)>>package.xml 
	done
}

writePackageFooter () {
	(	
		echo "${tab}<version>${apiVersion}</version>"
		echo "</Package>"
	)>>package.xml
}

#initialization
cd migration-tool/src/
tab="    "

apiVersion=36.0

#black list files
notDeployFiles[0]="Trial Customer Portal.portal"
#notDeployFiles[1]="SCMapViewController.cls-meta.xml"
#notDeployFiles[2]="ExternalEventMapping.workflow"
#notDeployFiles[3]="SocialPost-Social Post Layout.layout"

#white list folders
deploymentFolders[0]="classes"
deploymentFolders[1]="components"
deploymentFolders[2]="email"
deploymentFolders[3]="labels"
deploymentFolders[4]="layouts"
deploymentFolders[5]="objects"
deploymentFolders[6]="objectTranslations"
deploymentFolders[7]="pages"
deploymentFolders[8]="triggers"
deploymentFolders[9]="tabs"
deploymentFolders[10]="workflows"
deploymentFolders[11]="translations"
deploymentFolders[12]="quickActions"
deploymentFolders[13]="assignmentRules"
deploymentFolders[14]="dashboards"
deploymentFolders[15]="escalationRules"
deploymentFolders[16]="homePageComponents"
deploymentFolders[17]="homePageLayouts"
deploymentFolders[18]="reports"
deploymentFolders[19]="reportTypes"
deploymentFolders[20]="roles"
deploymentFolders[21]="sharingRules"
deploymentFolders[22]="staticresources"
deploymentFolders[23]="documents"
deploymentFolders[24]="appMenus"
deploymentFolders[25]="applications"
deploymentFolders[26]="customMetadata"

#deploymentFolders[22]="autoResponseRules"
#deploymentFolders[23]="communities"
#deploymentFolders[24]="datacategorygroups"
#deploymentFolders[25]="installedPackages"
#deploymentFolders[26]="matchingRules"

#deploymentFolders[29]="portals"


#types for package.xml
packageTypes[0]="ApexClass"
packageTypes[1]="ApexComponent"
packageTypes[2]="EmailTemplate"
packageTypes[3]="CustomLabel"
packageTypes[4]="Layout"
packageTypes[5]="CustomObject"
packageTypes[6]="CustomField"
packageTypes[7]="RecordType"
packageTypes[8]="CustomObjectTranslation"
packageTypes[9]="ApexPage"
packageTypes[10]="ApexTrigger"
packageTypes[11]="Workflow"
packageTypes[12]="ValidationRule"
packageTypes[13]="CustomTab"
packageTypes[14]="Translations"
packageTypes[15]="QuickAction"
packageTypes[16]="AssignmentRules"
packageTypes[17]="Dashboard"
packageTypes[18]="EscalationRules"
packageTypes[19]="HomePageComponent"
packageTypes[20]="HomePageLayout"
packageTypes[21]="Report"
packageTypes[22]="ReportType"
packageTypes[23]="Role"
packageTypes[24]="SharingRules"
packageTypes[25]="WebLink"
packageTypes[26]="WorkflowFieldUpdate"
packageTypes[27]="WorkflowRule"
packageTypes[28]="Document"
packageTypes[29]="ListView"
packageTypes[30]="StaticResource"
packageTypes[31]="AppMenu"
packageTypes[32]="CustomApplication"
packageTypes[33]="CustomMetadata"

#packageTypes[30]="Fieldexport"
#packageTypes[31]="AutoResponseRules"
#packageTypes[32]="Community"
#packageTypes[33]="DataCategoryGroup"
#packageTypes[34]="InstalledPackage"
#packageTypes[35]="MatchingRule"
#packageTypes[36]="MatchingRules"
#packageTypes[39]="Portal"



#Main
for d in *
do	
	removeInvalidFolderAndFiles $d	
done
createPackageXML
cd ..
